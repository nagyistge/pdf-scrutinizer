/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.Interpreter;
import org.mozilla.javascript.NativeObject;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.data.MethodVulnerability;
import de.pdf_scrutinizer.utils.Reflect;

public class Collab {
    private Log log = LogFactory.getLog(Interpreter.class);

    private final Scrutinizer scrutinizer;

    public Collab(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
    }

    public void collectEmailInfo(NativeObject o) {
        Reflect.getMethodName();
        int msg_length = o.get("msg", o).toString().length();
        if (msg_length > 0x1000) {
            MethodVulnerability vuln = new MethodVulnerability("CVE-2007-5659", "Collab.collectEmailInfo", "buffer overflow");
            scrutinizer.getAnalysisResult().vulnerabilityUsed(vuln);
        } else {
            //FIXME: debug
            log.debug("length:" + msg_length);
        }
    }

    public void getIcon(String str) {
        Reflect.getMethodName();

        if (str.length() > 0x4000 && str.contains("N.")) { //c[0] == 'N' && c[1] == '.') {
            MethodVulnerability vuln = new MethodVulnerability("CVE-2009-0927", "Collab.getIcon", "buffer overflow");
            scrutinizer.getAnalysisResult().vulnerabilityUsed(vuln);
        } else {
            //FIXME: debug
            log.debug("length: " + str.length());
            log.debug("string: " + str);
        }
    }
}
