/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.data.MethodVulnerability;
import de.pdf_scrutinizer.utils.Reflect;

public class Spell {
    private final Scrutinizer scrutinizer;

    public Spell(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
    }

    public boolean customDictionaryOpen(String cDIPath, String cName) {
        return customDictionaryOpen(cDIPath, cName, false);
    }

    public boolean customDictionaryOpen(String cDIPath, String cName, boolean bShow) {
        Reflect.getMethodName();
        if (cName.length() > 1024) {
            MethodVulnerability vuln = new MethodVulnerability("CVE-2009-1493", "spell.customDictionaryOpen", "buffer overflow");
            scrutinizer.getAnalysisResult().vulnerabilityUsed(vuln);
        }
        return true;
    }
}