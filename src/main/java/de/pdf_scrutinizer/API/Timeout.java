/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Timeout {
    private final Log log = LogFactory.getLog(Timeout.class);
    private final Runnable task;
    private final long delay;
    private final boolean interval;
    private boolean isActive;
    private FutureTask<?> future;
    private ScheduledExecutorService scheduledExecutorService;

    public Timeout(Runnable task, long delay) {
        this(task, delay, false);
    }

    public Timeout(Runnable task, long delay, boolean interval) {
        this.task = task;
        this.delay = delay;
        this.interval = interval;
        start();
    }

    public void start() {
        scheduledExecutorService = Executors.newScheduledThreadPool(1);

        if (!interval) {
            future = new FutureTask<Object>(task, null);
            scheduledExecutorService.schedule(future, delay, TimeUnit.MILLISECONDS);
        } else {
            future = new FutureTask<Object>(task, null);
            scheduledExecutorService.scheduleAtFixedRate(future, delay, delay, TimeUnit.MILLISECONDS);
        }
        isActive = true;

        log.debug("starting: " + this.toString());
    }

    public void stop() {
        // this makes sure, that the task has ran at least once.
        try {
            future.get();
        } catch (InterruptedException e) {
            log.warn(e.getMessage(), e);
        } catch (ExecutionException e) {
            log.warn(e.getMessage(), e);
        }

        future.cancel(false);
        isActive = false;
        scheduledExecutorService.shutdown();
        log.debug("stopping: " + this.toString());
    }

    @Override
    public String toString() {
        return String.format("timeout [type=%s, delay=%d active=%s]", (interval ? "interval" : "one-time"), delay, isActive);
    }
}
