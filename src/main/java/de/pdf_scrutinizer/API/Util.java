/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.Interpreter;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.data.MethodVulnerability;
import de.pdf_scrutinizer.utils.Reflect;

public class Util {
    private Log log = LogFactory.getLog(Interpreter.class);

    private final Scrutinizer scrutinizer;

    public Util(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
    }

    public void printd(String cFormat, Object oDate) {
        printd(cFormat, oDate, false);
    }

    /*
     * used along with media.newPlayer(null)-Exploit
     * 2 times before 1 time after. each time cFormat 36 chars = 72byte long.
     * 
     * 'p@111111111111111111111111 : yyyy111'
     */
    public void printd(String cFormat, Object oDate, boolean bXFAPicture) {
        Reflect.getMethodName();
        //System.out.println(cFormat.length()); 
    }

    public void printf(String cFormat) {
        printf(cFormat, null);
        //System.out.println(cFormat); //FIXME: debug?
    }

    public void printf(String cFormat, Object arguments) {
        Reflect.getMethodName();
        //FIXME: are these the only vulnerable strings?
        String[] vuln_strs = {"%45000.45000f", "%45000f"};

        if (Arrays.asList(vuln_strs).contains(cFormat)) {
            MethodVulnerability vuln = new MethodVulnerability("CVE-2008-2992", "Util.printf", "buffer overflow");
            scrutinizer.getAnalysisResult().vulnerabilityUsed(vuln);
        } else {
            log.debug("util.printf(\"" + cFormat + "\")");
        }
    }

    /*
     * not documented
     * used by 
     * 'CVE-2009-0927 CVE-2007-5659 PDF 2010-04-02_ IPR in China final.pdf=' 
     * md5: c497c02464ae74bbc94120d1cbe88d49
     */
    public char byteToChar(byte b) {
        return (char) b;
    }
}
