/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API.app;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.UniqueTag;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.API.Field;
import de.pdf_scrutinizer.API.app.doc.*;
import de.pdf_scrutinizer.data.MethodVulnerability;
import de.pdf_scrutinizer.document.DocumentAdapter;
import de.pdf_scrutinizer.utils.Reflect;

public abstract class Doc extends ScriptableObject {
    private static final long serialVersionUID = -5096572548458001033L;
    private final Log log = LogFactory.getLog(Doc.class);

    private static final Object THIS_TAG = "doc";
    private final Scrutinizer scrutinizer;

    public String URL;
    public int numPages;
    public boolean external;
    public Info info;
    public Console console = new Console();
    public double zoom = 100;

    protected Doc(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
        DocumentAdapter documentAdapter = scrutinizer.getDocumentAdapter();
        if (documentAdapter != null && documentAdapter.IsDocLoaded()) {
            numPages = documentAdapter.getDocument().getNumberOfPages();
        }

        info = new Info(scrutinizer);
    }

    public void syncAnnotScan() {
        Reflect.getMethodName();

        scrutinizer.getDocumentAdapter().syncAnnotScan();
    }

    public void printSeps() {
        Reflect.getMethodName();

        MethodVulnerability vuln = new MethodVulnerability("CVE-2010-4091", "printSeps", "memory corruption");
        scrutinizer.getAnalysisResult().vulnerabilityUsed(vuln);
    }

    public void getURL(Object cURL, Object bAppend) {
        Reflect.getMethodName();

        log.info(String.format("this is a stub: doc.getURL(%s, %b);", cURL, bAppend));
    }

    public Annotation[] getAnnots(Object o) {
        Reflect.getMethodName();

        Double page = null;
        if (o instanceof NativeObject) {
            NativeObject x = (NativeObject) o;
            if (x.get(0, x) != UniqueTag.NOT_FOUND && x.get(1, x) != UniqueTag.NOT_FOUND
                    && x.get(2, x) != UniqueTag.NOT_FOUND && x.get(3, x) != UniqueTag.NOT_FOUND) {
                double param1 = Double.parseDouble(x.get(0, x).toString());
                double param2 = Double.parseDouble(x.get(1, x).toString());
                double param3 = Double.parseDouble(x.get(2, x).toString());
                double param4 = Double.parseDouble(x.get(3, x).toString());

                if (param1 < 0 && param2 < 0 && param3 < 0 && param4 < 0) {
                    MethodVulnerability vuln = new MethodVulnerability("CVE-2009-1492", "getAnnots", "buffer overflow");
                    scrutinizer.getAnalysisResult().vulnerabilityUsed(vuln);
                }
            }

            page = Double.parseDouble(((NativeObject) o).get("nPage", (NativeObject) o).toString());
        } else if (o instanceof Integer) {
            page = Double.parseDouble(((Integer) o).toString());
        } else if (o instanceof Double) {
            page = (Double) o;
        } else {
            throw new RuntimeException("getAnnots type error");
        }

        return scrutinizer.getDocumentAdapter().getCachedAnnots(page.intValue());
    }
    
    /*
     * not possible at the moment because of the lack of overloading
     */
//    public Annotation getAnnot(NativeObject o) {
//    	Reflect.getMethodName();
//    	Double page = Double.parseDouble(o.get("nPage", o).toString());
//    	String name = o.get("cName", o).toString();
//        return scrutinizer.getDocumentAdapter().getAnnot(page.intValue(), name);
//    }

    public Annotation getAnnot(int nPage, String nName) {
        Reflect.getMethodName();

        DocumentAdapter documentAdapter = scrutinizer.getDocumentAdapter();
        return documentAdapter.getAnnot(nPage, nName);
    }

    /*
     * not possible at the moment because of the lack of overloading
     */
//    public String getPageNumWords() {
//        return Integer.toString(scrutinizer.getDocumentAdapter().getPageNumWords(0));
//    }

    public String getPageNumWords(Object pagex) {
        Reflect.getMethodName();

        if (pagex instanceof Double) {
            Double tmp = (Double) pagex;
            int p = tmp.intValue();
            return Integer.toString(scrutinizer.getDocumentAdapter().getPageNumWords(p));
        } else {
            NativeObject page = (NativeObject) pagex;
            //TODO:
            if (page.get(0, page) != UniqueTag.NOT_FOUND) {
                int p = Integer.parseInt(page.get(0, page).toString());
                return Integer.toString(scrutinizer.getDocumentAdapter().getPageNumWords(p));
            } else {
                return Integer.toString(scrutinizer.getDocumentAdapter().getPageNumWords(0));
            }
        }
    }
    
    /*
     * not possible at the moment because of the lack of overloading
     */
//    public String getPageNthWord() {
//        return getPageNthWord(0);
//    }

    public String getPageNthWord(int i, int j) {
        Reflect.getMethodName();

        DocumentAdapter documentAdapter = scrutinizer.getDocumentAdapter();
        return documentAdapter.getPageNthWord(i, j);
    }

    public void selectPageNthWord(int i, int j) {
        Reflect.getMethodName();
    }

    /*
     * does not really read field from PDF document.
     * TODO: some documents read shellcode from field. Implement!
     * Symantec: The rise of PDF malware, p. 10
     */
    public Field getField(String str) {
        Reflect.getMethodName();

        return new Field();
    }

    public boolean isBoxChecked(int id) {
        return false;
    }

    /*
     * I use this to set a breakpoint in my IDE, so I can put a breakpoint() 
     * call anywhere in a piece of code which was extracted but failed to execute
     * completely, in order to improve the emulation. 
     */
    public void breakpoint() {
    }

    @Override
    public String getClassName() {
        return (String) THIS_TAG;
    }
}