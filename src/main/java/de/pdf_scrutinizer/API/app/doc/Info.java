/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API.app.doc;

import java.io.IOException;
import java.util.Calendar;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.document.DocumentAdapter;

public class Info {
    public Info(Scrutinizer scrutinizer) {
        DocumentAdapter documentAdapter = scrutinizer.getDocumentAdapter();
        if (documentAdapter != null && documentAdapter.IsDocLoaded()) {
            PDDocument pd = documentAdapter.getDocument();
            PDDocumentInformation docinfo = pd.getDocumentInformation();
            if (docinfo != null) {
                Title = title = docinfo.getTitle();
                Author = author = docinfo.getAuthor();
                Subject = subject = docinfo.getSubject();
                Keywords = keywords = docinfo.getKeywords();
                Creator = creator = docinfo.getCreator();
                Producer = producer = docinfo.getProducer();
                try {
                    CreationDate = creationdate = docinfo.getCreationDate();
                    ModDate = moddate = docinfo.getModificationDate();
                } catch (IOException e) {
                    // should already be triggered at  de.pdf_scrutinizer.DocumentAdapter.getDocInfo
                }
                Trapped = trapped = docinfo.getTrapped();
            }
        }
    }

    public String Title;
    public String title;
    public String Author;
    public String author;
    public String Subject;
    public String subject;
    public String Keywords;
    public String keywords;
    public String Creator;
    public String creator;
    public String Producer;
    public String producer;
    public Calendar CreationDate;
    public Calendar creationdate;
    public Calendar ModDate;
    public Calendar moddate;
    public String Trapped; //boolean?
    public String trapped;
}
