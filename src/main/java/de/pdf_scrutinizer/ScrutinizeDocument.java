/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import de.pdf_scrutinizer.utils.*;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import de.pdf_scrutinizer.data.AnalysisResult;
import de.pdf_scrutinizer.dynamic_heuristics.LibemuShellcodeTester;

public class ScrutinizeDocument {
    private static Options options;

    private static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(100);
        formatter.printHelp("./run.sh -pdf malicious.pdf [-js test.js] [-libemuPath /opt/libemu/sctest]",
                "PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.",
                options,
                "For more information visit https://bitbucket.org/florianschmitt/pdf-scrutinizer");
    }

    public static void main(String args[]) {
        InputStream in = Input.findFileInClasspath("log4j.properties");

        if (in != null) {
            Properties p = new Properties();
            try {
                p.load(in);
                PropertyConfigurator.configure(p);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            BasicConfigurator.configure();
        }

        options = new Options();
        options.addOption("pdf", true, "the document to be analyzed");
        options.addOption("js", true, "javascript file which is executed (for debug)");
        options.addOption("libemuPath", true, "path to libemu library (sctest binary actually...)");
        options.addOption("mongo", true, "save in mongodb, provide the url_id");

        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            printHelp();
            System.err.println(e.getMessage());
            return;
        }

        if (cmd.hasOption("js") && cmd.hasOption("pdf")) {
            Scrutinizer s = new Scrutinizer();
            try {
                s.setRootDocument(new File(cmd.getOptionValue("pdf")));
                s.setJSCode(new File(cmd.getOptionValue("js")));
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
                return;
            }
            s.setOutput(new OutputNull());
            s.runJSCodeAndPDF();
            System.out.println(AnalysisResultHelper.toString(s.getAnalysisResult()));
        } else if (cmd.hasOption("js")) {
            Scrutinizer s = new Scrutinizer();
            try {
                s.setRootDocument(new File(cmd.getOptionValue("js")));
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
                return;
            }
            s.setOutput(new OutputNull());
            s.runJSCode();
        } else if (cmd.hasOption("pdf") && cmd.hasOption("mongo")) {
            Scrutinizer scrutinizer = new Scrutinizer();
            try {
                scrutinizer.setRootDocument(new File(cmd.getOptionValue("pdf")));
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
                return;
            }

            OutputToMongoDB output = new OutputToMongoDB(scrutinizer, cmd.getOptionValue("mongo"));
            scrutinizer.setOutput(output);

            if (cmd.hasOption("libemuPath")) {
                LibemuShellcodeTester libemuTester = null;
                try {
                    libemuTester = new LibemuShellcodeTester(scrutinizer, cmd.getOptionValue("libemuPath"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                if (libemuTester != null) {
                    scrutinizer.getDynamicHeuristics().setShellcodeTester(libemuTester);
                }
            }

            scrutinizer.analyze();

        } else if (cmd.hasOption("pdf")) {
            Scrutinizer scrutinizer = new Scrutinizer();
            try {
                scrutinizer.setRootDocument(new File(cmd.getOptionValue("pdf")));
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
                return;
            }

            OutputToFiles output = new OutputToFiles(scrutinizer, scrutinizer.getHash(), "result");
            output.setSaveDynamicICode(false);
            output.setShowHexdump(false);
            scrutinizer.setOutput(output);

            if (cmd.hasOption("libemuPath")) {
                LibemuShellcodeTester libemuTester = null;
                try {
                    libemuTester = new LibemuShellcodeTester(scrutinizer, cmd.getOptionValue("libemuPath"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                if (libemuTester != null) {
                    scrutinizer.getDynamicHeuristics().setShellcodeTester(libemuTester);
                }
            }

            AnalysisResult result = scrutinizer.analyze();
            System.out.println(AnalysisResultHelper.toString(result));
        } else {
            printHelp();
        }
    }
}