/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class AnalysisResult implements Serializable {
    private static final long serialVersionUID = -121219000071752870L;

    public enum Classification {
        benign, suspicious, malicious
    }

    public final Date analysisStart = new Date();
    public Date analysisEnd;
    public String filename;
    public String hash;
    public List<String[]> embeddedFiles;
    public List<String> codes;
    public Classification classification = Classification.benign;
    public Boolean codeFound = false;
    public Boolean error = false;
    private List<String> fulfilledHeuristics = new LinkedList<String>();
    private List<Vulnerability> foundVulnerabilities = new LinkedList<Vulnerability>();
    private List<Exception> exceptions;

    public static AnalysisResult error() {
        AnalysisResult result = new AnalysisResult("", "");
        result.analysisEnd = new Date();
        result.error = true;
        return result;
    }

    /**
     * GWT needs a blank constructor... hopefully this one is never called...
     */
    @SuppressWarnings("unused")
    private AnalysisResult() {
    }

    public AnalysisResult(String filename, String hash) {
        this.filename = filename;
        this.hash = hash;
    }

    public synchronized void vulnerabilityUsed(Vulnerability vulnerability) {
        foundVulnerabilities.add(vulnerability);
        classification = Classification.malicious;
    }

    public synchronized void heuristicFulfilled(String heuristic) {
        if (!fulfilledHeuristics.contains(heuristic)) {
            fulfilledHeuristics.add(heuristic);
        }
        classification = Classification.malicious;
    }

    public void suspicious() {
        if (classification == Classification.benign) {
            classification = Classification.suspicious;
        }
    }

    public synchronized void addException(Exception e) {
        if (exceptions == null) {
            exceptions = new LinkedList<Exception>();
        }
        exceptions.add(e);
    }

    @JsonIgnore
    public String getFulfilledHeuristicsString() {
        String result = "";
        for (String x : fulfilledHeuristics) {
            result += x + " ";
        }
        return result;
    }

    @JsonIgnore
    public String getVulnerabilitiesString() {
        String result = "";
        for (Vulnerability x : foundVulnerabilities) {
            result += x.getCVEID() + " ";
        }
        return result;
    }

    public List<Vulnerability> getUsedVulnerabilities() {
        return foundVulnerabilities;
    }

    public Classification getClassification() {
        return classification;
    }

    public List<String> getFulfilledHeuristics() {
        return fulfilledHeuristics;
    }

    public List<Exception> getExceptions() {
        return exceptions;
    }
}