/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.document;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.pdf_scrutinizer.API.app.doc.Annotation;

public class DocumentAnnotationScanTask implements Runnable {
    private final Log log = LogFactory.getLog(DocumentAnnotationScanTask.class);
    private final DocumentAdapter adapter;
    private final HashMap<Integer, Annotation[]> result = new HashMap<Integer, Annotation[]>();

    public DocumentAnnotationScanTask(DocumentAdapter adapter) {
        this.adapter = adapter;
    }

    public HashMap<Integer, Annotation[]> getResult() {
        return result;
    }

    @Override
    public void run() {
        log.debug("starting annotation scan");
        int numPages = adapter.getDocument().getNumberOfPages();
        for (int i = 0; i < numPages; i++) {
            log.debug("starting page " + i);
            result.put(i, adapter.getAnnots(i));
            log.debug("finished page " + i);
        }
        log.debug(String.format("finished annotation scan, found %d annotation%s", result.size(), (result.size() == 1 ? "" : "s")));
    }
}