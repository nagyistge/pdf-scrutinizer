/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.dynamic_heuristics;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.pdf_scrutinizer.Scrutinizer;

public class LibemuShellcodeTester implements ShellcodeTester {
    private Log log = LogFactory.getLog(LibemuShellcodeTester.class);

    public final String LIBEMU_DEFAULT_OPTIONS = "-v -s 1000000 -g -S";
    @SuppressWarnings("unused")
    public final String LIBEMU_SCDBG_DEFAULT_OPTIONS = "/nc /getpc /findsc /S";
    @SuppressWarnings("unused")
    public final String LIBQEMU_DEFAULT_OPTIONS = "-ivS -g -s 2000000";

    private final Scrutinizer scrutinizer;
    private boolean showShellcodeTesterOutput = false;
    private boolean saveShellcode = true;

    private final String libemuPath;
    private final String libemuOptions;

    public LibemuShellcodeTester(Scrutinizer scrutinizer, String libemuPath) throws FileNotFoundException {
        this.scrutinizer = scrutinizer;
        this.libemuPath = libemuPath;
        if (!new File(libemuPath).exists()) {
            throw new FileNotFoundException("libemu path was not correct: " + libemuPath);
        }
        this.libemuOptions = LIBEMU_DEFAULT_OPTIONS;
    }

    public LibemuShellcodeTester(Scrutinizer scrutinizer, String libemuPath, String libemuOptions) throws FileNotFoundException {
        this.scrutinizer = scrutinizer;
        this.libemuPath = libemuPath;
        if (!new File(libemuPath).exists()) {
            throw new FileNotFoundException("libemu path was not correct: " + libemuPath);
        }
        this.libemuOptions = libemuOptions;
    }

    public void setShowOutput(Boolean b) {
        showShellcodeTesterOutput = b;
    }

    public void setSaveShellcode(Boolean b) {
        saveShellcode = b;
    }

    public boolean getShowOutput() {
        return showShellcodeTesterOutput;
    }

    public boolean getSaveShellcode() {
        return saveShellcode;
    }

    public void testShellcode(String str) {
        scrutinizer.getOutput().prettyPrint(str);

        StringBuilder sb = new StringBuilder();
        BufferedWriter out = null;
        BufferedReader inp = null;
        String line = "";
        int stepcount = 0;
        String libemucmd = libemuPath + " " + libemuOptions;

        try {
            Process p = Runtime.getRuntime().exec(libemucmd);
            out = new BufferedWriter(new OutputStreamWriter(p.getOutputStream(), "UTF-16LE"));  //Scrutinizer -> LIBEMU
            inp = new BufferedReader(new InputStreamReader(p.getInputStream()));                //LIBEMU -> Scrutinizer

            out.write(str);
            out.close();

            while ((line = inp.readLine()) != null) {
                sb.append(line + "\n");
                if (line.toLowerCase().contains("stepcount")) {
                    Matcher m = Pattern.compile("\\d+").matcher(line);
                    if (m.find()) {
                        stepcount = Integer.parseInt(line.substring(m.start(), m.end()));
                    }
                }
            }
        } catch (IOException e) {
            log.error("", e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (inp != null) {
                    inp.close();
                }
            } catch (IOException e) {
                log.error("", e);
            }
        }

        if (getShowOutput()) {
            log.info("libemu output:");
            System.out.println(sb.toString().trim());
        }

        if (stepcount > 1000) {
            scrutinizer.getAnalysisResult().heuristicFulfilled("ShellcodeTester");
            log.info("probably shellcode found, stepcount: " + stepcount);
            scrutinizer.getOutput().saveShellcode(str, sb.toString().trim());
        }
    }
}
