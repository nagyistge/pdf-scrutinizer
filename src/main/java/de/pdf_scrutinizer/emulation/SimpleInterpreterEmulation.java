/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.emulation;

import java.util.List;

import org.mozilla.javascript.ScriptableNativeJavaObject;

import de.pdf_scrutinizer.Scrutinizer;

public class SimpleInterpreterEmulation extends StandaloneInterpreterEmulation {
    public SimpleInterpreterEmulation(Scrutinizer scrutinizer) {
        super(scrutinizer);
        viewerversions = new String[]{"8"};
    }

    @Override
    public void execute(final List<String> code) {
        if (document == null) {
            log.error("doc-object of InterpreterEmulation is not initialized");
            return;
        }

        for (String x : viewerversions) {
            new ScriptableNativeJavaObject.ScriptableNativeContextFactory()
                    .call(generateContextAction(x, "5.13", code));
        }
    }
}