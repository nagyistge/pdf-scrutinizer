/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.exposures;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;

import de.pdf_scrutinizer.Scrutinizer;

public class DocumentExposureScanTask implements Runnable {
    private Log log = LogFactory.getLog(DocumentExposureScanTask.class);
    private final List<DocumentExposure> exposures = new ArrayList<DocumentExposure>();
    private final PDDocument document;

    public DocumentExposureScanTask(Scrutinizer scrutinizer, PDDocument document) {
        this.document = document;

        exposures.add(new CVE_2009_0658(scrutinizer));
        exposures.add(new CVE_2010_0188(scrutinizer));
    }

    @Override
    public void run() {
        log.debug("starting document exposures scan");
        for (DocumentExposure x : exposures) {
            x.run(this.document);
        }
        log.debug("finished document exposures scan");
    }
}