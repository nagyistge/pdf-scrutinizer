/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.lang.time.DateFormatUtils;

import de.pdf_scrutinizer.data.AnalysisResult;
import de.pdf_scrutinizer.data.Vulnerability;

public class AnalysisResultHelper {

    private static String getAnalysisTimeString(AnalysisResult analysisResult) {
        long diff = analysisResult.analysisEnd.getTime() - analysisResult.analysisStart.getTime();
        return DateFormatUtils.format(diff, "mm:ss");
    }

    public static String toString(AnalysisResult analysisResult) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%-25s%s\n", "Analysis start:", analysisResult.analysisStart));
        sb.append(String.format("%-25s%s\n", "Analysis end:", analysisResult.analysisEnd));
        sb.append(String.format("%-25s%s\n", "Analysis time:", getAnalysisTimeString(analysisResult)));
        if (analysisResult.error) {
            if (analysisResult.getExceptions() != null && analysisResult.getExceptions().size() > 0) {
                sb.append("exceptions raised:\n");
                for (Exception x : analysisResult.getExceptions()) {
                    StringWriter sw = new StringWriter();
                    x.printStackTrace(new PrintWriter(sw));
                    sb.append(sw.toString());
                }
                sb.append("\n");
            }
            sb.append("Error occurred\n");
            return sb.toString();
        }
        sb.append(String.format("%-25s%s\n", "Filename:", analysisResult.filename));
        sb.append(String.format("%-25s%s\n", "MD5 Hash:", analysisResult.hash));
        if (analysisResult.embeddedFiles != null && analysisResult.embeddedFiles.size() > 0) {
            sb.append(String.format("%-25s%d\n", "Embedded files count:", analysisResult.embeddedFiles.size()));
            int i = 1;
            for (String[] x : analysisResult.embeddedFiles) {
                if (x[0] != null) {
                    sb.append(String.format("Embed file %d %s\n", i++, x[0]));
                }
            }
        }

        sb.append(String.format("%-25s%b\n", "JavaScript events:", analysisResult.codeFound));
        if (analysisResult.codeFound) {
            sb.append(String.format("%-25s%d\n", "JavaScript count:", analysisResult.codes.size()));
        }

        if (analysisResult.getFulfilledHeuristics().size() > 0) {
            StringBuilder sb2 = new StringBuilder();
            for (String x : analysisResult.getFulfilledHeuristics()) {
                sb2.append(x + " ");
            }
            sb.append(String.format("%-25s%s\n", "heuristics raised:", sb2.toString()));
        }

        if (analysisResult.getUsedVulnerabilities().size() > 0) {
            sb.append("vulnerabilities found:\n");
            for (Vulnerability x : analysisResult.getUsedVulnerabilities()) {
                sb.append(String.format("\n%s\n", x));
            }
        }

        if (analysisResult.getExceptions() != null && analysisResult.getExceptions().size() > 0) {
            sb.append("exceptions raised:\n");
            for (Exception x : analysisResult.getExceptions()) {
                StringWriter sw = new StringWriter();
                x.printStackTrace(new PrintWriter(sw));
                sb.append(sw.toString());
            }
            sb.append("\n");
        }

        sb.append(String.format("%-25s%s", "Classification:", analysisResult.classification));
        return sb.toString();
    }
}
