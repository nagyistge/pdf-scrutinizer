/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.utils;

import java.util.HashMap;

public class Benchmark {
    long scrutinizercumulative = 0;
    long scrutinizerstart;

    long shellcodecumulative = 0;
    long shellcodestart;

    public void shellcodeStart() {
        shellcodestart = System.currentTimeMillis() / 1000;
    }

    public void shellcodeStop() {
        if (shellcodestart > 0) {
            long shellcodestop = System.currentTimeMillis() / 1000;
            shellcodecumulative += shellcodestop - shellcodestart;
            shellcodestart = 0;
        }
    }

    public void scrutinizerStart() {
        scrutinizerstart = System.currentTimeMillis() / 1000;
    }

    public void scrutinizerStop() {
        if (scrutinizerstart > 0) {
            long scrutinizerstop = System.currentTimeMillis() / 1000;
            scrutinizercumulative += scrutinizerstop - scrutinizerstart;
            scrutinizerstart = 0;
        }
    }

    public HashMap<String, Long> getBenchmarkReport() {
        HashMap<String, Long> result = new HashMap<String, Long>();
        result.put("overall", scrutinizercumulative);
        result.put("shellcode", shellcodecumulative);
        return result;
    }
}
