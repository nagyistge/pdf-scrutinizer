/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.pdf_scrutinizer.Scrutinizer;

public class Input {
    private static Log log = LogFactory.getLog(Input.class);
    private static final String resourcesDirectory = "resources";
    public static final String REGEX_MALICIOUS = "RegexMalicious.txt";
    public static final String REGEX_SUSPICIOUS = "RegexSuspicious.txt";
    public static final String VULNERABLE_API_CALLS = "VulnerableAPICalls.txt";

    public static String readFile(File file) {
        StringBuilder result = new StringBuilder();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            line = br.readLine();

            while (line != null) {
                result.append(line + "\n");
                line = br.readLine();
            }

        } catch (FileNotFoundException e) {
            log.warn("", e);
        } catch (IOException e) {
            log.warn("", e);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                log.warn(e);
            }
        }

        return result.toString();
    }

    public static String readFile(String filename) {
        return readFile(new File(filename));
    }

    public static File downloadFile(URL url) {
        String pdffilename = null;

        try {
            pdffilename = url.getFile();
            pdffilename = pdffilename
                    .substring(pdffilename.lastIndexOf("/") + 1);
            InputStream input = url.openStream();
            FileOutputStream output = new FileOutputStream(pdffilename);

            int read = 0;
            byte[] buffer = new byte[1024];

            while ((read = input.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }

            input.close();
            output.flush();
            output.close();
        } catch (MalformedURLException e) {
            log.fatal("problem while downloading file from url ", e);
        } catch (IOException e) {
            log.warn("", e);
        }

        return new File(pdffilename);
    }

    /**
     * Tries to find 'filename' in the classpath (jar-file). But it priorities
     * files located in 'resoures/filename' towards the same file included in
     * the jar. This way we can have a default config file in the jar but we can
     * overwrite/replace it in a productive system. MIND CLOSING THE STREAM
     * AFTER USE!
     *
     * @param filename
     * @return
     */
    @SuppressWarnings("resource")
    public static InputStream findFileInClasspath(String filename) {
        InputStream result = null;

        File file = new File(resourcesDirectory + File.separator + filename);
        if (file.exists()) {
            try {
                result = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            result = Scrutinizer.class.getClassLoader().getResourceAsStream(
                    filename);
        }

        return result;
    }
}