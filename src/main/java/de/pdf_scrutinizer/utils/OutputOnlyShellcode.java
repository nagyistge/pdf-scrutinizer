/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.dynamic_heuristics.ShellcodeTester;

public class OutputOnlyShellcode extends OutputNull {
    private static Log log = LogFactory.getLog(OutputOnlyShellcode.class);
    private final Scrutinizer scrutinizer;

    public OutputOnlyShellcode(Scrutinizer scrutinizer, String prefix,
                               String resultFolder) {
        this.scrutinizer = scrutinizer;

        if (!new File(resultFolder).exists()) {
            new File(resultFolder).mkdir();
        }

        String formatString = "%s%s%s_%s";
        filename_shellcode = String.format(formatString, resultFolder,
                File.separator, prefix, filename_shellcode);
    }

    private String filename_shellcode = "shellcode";

    private int sccount = 0;

    @Override
    public void saveShellcode(String sc, String stdout) {
        ShellcodeTester tester = scrutinizer.getDynamicHeuristics()
                .getShellcodeTester();
        if (tester != null && tester.getSaveShellcode()) {
            BufferedWriter fileWriter_shellcode = null;
            try {
                fileWriter_shellcode = new BufferedWriter(
                        new OutputStreamWriter(new FileOutputStream(
                                filename_shellcode + sccount + ".bin"),
                                "UTF-16LE"));
                fileWriter_shellcode.write(sc);
                fileWriter_shellcode.close();
                fileWriter_shellcode = new BufferedWriter(
                        new OutputStreamWriter(new FileOutputStream(
                                filename_shellcode + sccount++ + ".info")));
                fileWriter_shellcode.write(stdout);
            } catch (UnsupportedEncodingException e) {
                log.warn(e);
            } catch (FileNotFoundException e) {
                log.warn(e);
            } catch (IOException e) {
                log.warn(e);
            } finally {
                try {
                    fileWriter_shellcode.close();
                } catch (IOException e) {
                    log.warn(e);
                }
            }
        }
    }
}