/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer;

import org.junit.Test;
import de.pdf_scrutinizer.data.AnalysisResult;
import de.pdf_scrutinizer.utils.AnalysisResultHelper;
import de.pdf_scrutinizer.utils.OutputNull;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class TestAcceptance {

    public static AnalysisResult getAnalysisResult(String path) {
        Scrutinizer s = new Scrutinizer();
        File f = new File(path);
        assertTrue(f.exists());

        try {
            s.setRootDocument(f);
        } catch (FileNotFoundException e) { }

        s.setOutput(new OutputNull());
        AnalysisResult result = s.analyze();
        System.out.println(AnalysisResultHelper.toString(result));
        return result;
    }

    @Test
    public void testCVE_2009_4324() {
        String sample = "src/test/resources/2Collection/CVE-2009-4324_PDF_2009-11-30_note200911.pdf=1ST0DAYFILE";
        AnalysisResult result = getAnalysisResult(sample);

        assertEquals(result.getClassification(), AnalysisResult.Classification.malicious);
        assertEquals(result.getUsedVulnerabilities().get(0).getCVEID(), "CVE-2009-4324");
        assertEquals(result.getFulfilledHeuristics().get(0), "HeapSprayDetector");
    }

    @Test
    public void testCVE_2010_0188() {
        String sample = "src/test/resources/2Collection/CVE-2010-0188_PDF_2010-03-09_invitation.pdf=1ST0DAYFILE";
        AnalysisResult result = getAnalysisResult(sample);

        assertEquals(result.getClassification(), AnalysisResult.Classification.malicious);
        assertEquals(result.getUsedVulnerabilities().get(0).getCVEID(), "CVE-2010-0188");
    }

    @Test
    public void testCVE_2010_1297() {
        String sample = "src/test/resources/2Collection/CVE-2010-1297_PDF_fca0277b807433a437553113bf702160ccb365e.pdf=1ST0DAYFILE";
        AnalysisResult result = getAnalysisResult(sample);

        assertEquals(result.getClassification(), AnalysisResult.Classification.malicious);
        assertEquals(result.getFulfilledHeuristics().get(0), "HeapSprayDetector");
    }

    @Test
    public void testCVE_2010_2883() {
        String sample = "src/test/resources/2Collection/CVE-2010-2883_PDF_2010-09-06_GolfClinic.pdf=1ST0DAYFILE";
        AnalysisResult result = getAnalysisResult(sample);

        assertEquals(result.getClassification(), AnalysisResult.Classification.malicious);
        assertEquals(result.getFulfilledHeuristics().get(0), "HeapSprayDetector");
    }

    @Test
    public void testCVE_2010_3654() {
        String sample = "src/test/resources/2Collection/CVE-2010-3654_PDF_2010-10-26_NewsRelease.pdf=1ST0DAYFILE";
        AnalysisResult result = getAnalysisResult(sample);

        assertEquals(result.getClassification(), AnalysisResult.Classification.malicious);
        assertEquals(result.getFulfilledHeuristics().get(0), "HeapSprayDetector");
    }

    @Test
    public void testCVE_2010_4091() {
        String sample = "src/test/resources/2Collection/CVE-2010-4091_PDF_2010-11-04_xpl_pdf.pdf=1ST0DAYFILE";
        AnalysisResult result = getAnalysisResult(sample);

        assertEquals(result.getClassification(), AnalysisResult.Classification.malicious);
        assertEquals(result.getUsedVulnerabilities().get(0).getCVEID(), "CVE-2010-4091");
        assertEquals(result.getFulfilledHeuristics().get(0), "HeapSprayDetector");
    }
}